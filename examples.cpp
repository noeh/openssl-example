#include "crypto.hpp"
#include <iostream>
#include <bitset>
#include <string>

using namespace std;

bool isPasswordEqual(string word) {
    if (Crypto::hex(Crypto::pbkdf2(word, "Saltet til Ola", 2048, 160)) == "ab29d7b5c589e18b52261ecba1d3a7e7cbf212c6")
        return true;
    else
        return false;
}

bool getAllPermutationsTested(char word[], int len, int start) {
    if(start == len) {
        cout << "Password: " << string(word) << endl;
        return Crypto::hex(Crypto::pbkdf2(string(word), "Saltet til Ola", 2048, 160)) == "ab29d7b5c589e18b52261ecba1d3a7e7cbf212c6";
    }
    for(int i = 65; i<91; i++) {
        word[start] = i;
        if(getAllPermutationsTested(word, len, start+1)) {
            return true;
        }
    }
    for (int i = 97; i < 123; i++) {
        word[start] = i;
        if(getAllPermutationsTested(word, len, start+1)){
            return true;
        }
    }

    return false;
}
        

int main() {
  cout << "SHA-1 with 1 iteration" << endl;
  cout << Crypto::hex(Crypto::sha1("Test")) << endl << endl;

  cout << "SHA-1 with two iterations" << endl;
  cout << Crypto::hex(Crypto::sha1(Crypto::sha1("Test"))) << endl << endl;
  
  cout << "SHA-256 with 1 iteration" << endl;
  cout << Crypto::hex(Crypto::sha256("Test")) << endl << endl;

  cout << "SHA-256 with two iterations" << endl;
  cout << Crypto::hex(Crypto::sha256(Crypto::sha256("Test"))) << endl << endl;

  cout << "SHA-256 intermediate iterations" << endl << endl;
  cout << Crypto::hex(Crypto::sha256("Test")) << endl;

  cout << "MD-5 with 1 iteration" << endl;
  cout << Crypto::hex(Crypto::md5("Test")) << endl << endl;

  cout << "The derived key from the PBKDF2 algorithm" << endl;
  cout << Crypto::hex(Crypto::pbkdf2("Password", "Salt")) << endl;
  
  bool found = false;
  int len = 0;
  while(!found) {
    len += 1;
    char word[len + 1]; 
    word[len] = '\0';
    found = getAllPermutationsTested(word, len, 0);
    if(found)  
       cout<<"The final password is: " << string(word) << endl;
  }
}
